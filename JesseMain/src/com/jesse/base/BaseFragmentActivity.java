package com.jesse.base;

import com.jesse.util.AppUtil;
import com.jesse.util.ViewUtil;
import com.jesse.custom.asyncimageview.AsyncImageLoader;
import com.jesse.dao.ButtonDao;
import com.jesse.main.R;
import com.jesse.observer.DefaultEvent;
import com.jesse.observer.ObserverBus;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.MotionEvent;

public class BaseFragmentActivity extends FragmentActivity{

	private float xDown;
	private float xUp;
	private boolean isOverFIXED;
	private final int FIXED = (int) (ViewUtil.getScreenWidth() * 0.2);
	public boolean gestrue = true;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_base_fragment);
		ViewUtil.setContext(this);
		registerReceiver();
	}
	
	@Override
    public void onResume() {
        super.onResume();
        checkGuider();
        ViewUtil.setCurrentContext(this);
    }
	
	public void checkGuider() {
		
	}
	
	//手勢开关
	public boolean isGestrue() {
		return gestrue;
	}
	
	public void setGestrue(boolean gestrue) {
		this.gestrue = gestrue;
	}
	
	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {
		int action = event.getAction();
		switch (action) {
		case MotionEvent.ACTION_DOWN:
			xDown = event.getX();
			break;
		case MotionEvent.ACTION_UP:
			xDown = 0;
			xUp = 0;
			break;
		case MotionEvent.ACTION_MOVE:
			xUp = event.getX();
			isOverFIXED = xUp - xDown > FIXED ? true : false;
			if (isOverFIXED && gestrue && xDown < ViewUtil.getScreenWidth() / 8) {
				finish();
				return true;
			}
			break;
		}
		return super.dispatchTouchEvent(event);
	}
	
	private void registerReceiver() {
		IntentFilter mFilter = new IntentFilter();
        mFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        this.registerReceiver(mReceiver, mFilter);
	}
	
	private BroadcastReceiver mReceiver = new BroadcastReceiver() {
	 	
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            try {
	            if (action.equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
	            	checkNetStateAvailable();
	            }
            } catch (java.lang.RuntimeException e){
            	
            }
        }
    };
    
    public boolean checkNetStateAvailable() {
    	ConnectivityManager connectivityManager = (ConnectivityManager) ViewUtil.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
    	NetworkInfo info = connectivityManager.getActiveNetworkInfo(); 
    	NetworkInfo mobileInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);  
        NetworkInfo wifiInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);  
        ViewUtil.Debug("收到广播");
        if (info != null) {
        	 ViewUtil.Debug("NetworkInfo isAvailable is " + info.isAvailable());
             ViewUtil.Debug("NetworkInfo isConnected is " + info.isConnected());
             ViewUtil.Debug("NetworkInfo isConnectedOrConnecting is " + info.isConnectedOrConnecting());
        }
        if (mobileInfo != null) {
        	 ViewUtil.Debug("mobileInfo isAvailable is " + mobileInfo.isAvailable());
             ViewUtil.Debug("mobileInfo isConnected is " + mobileInfo.isConnected());
             ViewUtil.Debug("mobileInfo isConnectedOrConnecting is " + mobileInfo.isConnectedOrConnecting());
        }
       
        if (wifiInfo != null) {
        	 ViewUtil.Debug("wifiInfo isAvailable is " + wifiInfo.isAvailable());
             ViewUtil.Debug("wifiInfo isConnected is " + wifiInfo.isConnected());
             ViewUtil.Debug("wifiInfo isConnectedOrConnecting is " + wifiInfo.isConnectedOrConnecting());
        }
        if(info == null || !info.isAvailable()) {
        	netError();
        	return false;
        } else if(info != null || info.isConnected()){
    	    netRecover();
           return true;
        }
    	return false;
    }
    
    public synchronized void netError() {
    	if (AppUtil.netStateOk) {
    		ViewUtil.Debug("网络不可用");
    		AppUtil.netStateOk = false;
    		popupNetSetting();
        	ObserverBus.getInstance().updateEvent(DefaultEvent.NETSTATE, false);
    	}
    }
    
    public synchronized void netRecover() {
    	if (!AppUtil.netStateOk) {
    		ViewUtil.Debug("网络可用");
    		AppUtil.netStateOk = true;
        	ObserverBus.getInstance().updateEvent(DefaultEvent.NETSTATE, true);
    	}
    }
    
    private void popupNetSetting() {
    	ViewUtil.popupWithTwoButton(getString(R.string.netError), getString(R.string.alert), getString(R.string.netSetting), 
    			new ButtonDao() {
					
					@Override
					public void onClick() {
						Intent intent = new Intent(android.provider.Settings.ACTION_SETTINGS);
						startActivity(intent);
					}
					
				}, ViewUtil.getContext().getString(R.string.cancel), new ButtonDao() {
					
					@Override
					public void onClick() {
					}
				});
    }
    
	public void finish() {
		ViewUtil.endWaiting();
		ViewUtil.closePopup();
		super.finish();
		overridePendingTransition(0, R.anim.out_to_left);
	}
	
	@Override
    protected void onDestroy() {
		ViewUtil.removeContext(this);
        ViewUtil.hideKeyBoard();
        this.unregisterReceiver(mReceiver);
        super.onDestroy();
    }
}
