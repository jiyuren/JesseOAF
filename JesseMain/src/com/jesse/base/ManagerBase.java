package com.jesse.base;

import java.util.Observable;

import com.jesse.util.ViewUtil;

public abstract class ManagerBase extends Observable{
	 
	public abstract void releaseManager();
	public abstract void resetPool();
	
	public ManagerBase() {
		ViewUtil.addManamger(this.getClass().getName(), this);
	}
	
	/**
	 * 观察者通知接口
	 * @param object
	 */
	public void notifycation(Object object) {
		setChanged();
		notifyObservers(object);
	}
}
