package com.jesse.dao;

public interface InputTextDao {
	void textChange(String text);
	void textInputComplete(String text);
}
