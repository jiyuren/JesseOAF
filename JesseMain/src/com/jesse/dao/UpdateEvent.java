package com.jesse.dao;

public interface UpdateEvent {
	public void update(Object event);
}
