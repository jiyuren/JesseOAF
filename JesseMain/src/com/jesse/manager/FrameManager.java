package com.jesse.manager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.jesse.dao.FragmentContentChangeDaoComplete;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

public class FrameManager {
	private Map<String, Fragment> fragmentPool;
	private FragmentManager fragmentManager;
	private Map<String, List<FragmentContentChangeDaoComplete>> changeCompletes;
	private int mainContainer;
	private String currentFragmentKey = "";
	
	private FrameManager() {
		fragmentPool = new HashMap<String, Fragment>();
		changeCompletes = new HashMap<String, List<FragmentContentChangeDaoComplete>>();
	}
	
	private static FrameManager frameManager = null;
	
	public synchronized static FrameManager getInstence() {
		if (frameManager == null) {
			frameManager = new FrameManager();
		}
		return frameManager;
	}
	
	public void release() {
		fragmentPool.clear();
		changeCompletes.clear();
		frameManager = null;
	}
	
	/**
	 * 获取当前显示Fragment 的Key值
	 * @return
	 */
	public String getCurrentFragmentKey() {
		return currentFragmentKey;
	}

	public void init(FragmentManager fragmentManager, int mainContainer) {
		this.fragmentManager = fragmentManager;
		this.mainContainer = mainContainer;
	}
	
	public void addFragment(String key, Fragment fragment) {
		fragmentPool.put(key, fragment);
	}
	
	public void addAndShowFragment(String key, Fragment fragment) {
		addFragment(key, fragment);
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
	    fragmentTransaction.add(mainContainer, fragment).commitAllowingStateLoss(); 
	}

	public void addAndShowFragment(String key, Fragment fragment, FragmentContentChangeDaoComplete changeComplete) {
		addFragment(key, fragment);
		addListener(key, changeComplete);
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
	    fragmentTransaction.add(mainContainer, fragment).commitAllowingStateLoss(); 
	}
	
	public void addListener(String key, FragmentContentChangeDaoComplete changeComplete) {
		if (changeCompletes.get(key) == null) {
			changeCompletes.put(key, new ArrayList<FragmentContentChangeDaoComplete>());
		}
		List<FragmentContentChangeDaoComplete> array = changeCompletes.get(key);
		array.add(changeComplete);
	}
	
	public void selectItem(String key) {  
		hideFragment();
		if (fragmentPool.get(key) == null) {    
			throw new NullPointerException();
		} else {
		    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		    fragmentTransaction.show(fragmentPool.get(key)).commitAllowingStateLoss(); 
		    currentFragmentKey = key;
		}
		nofityChangeComplete(key);
    }
	
	private void hideFragment() {
		for (Entry<String, Fragment> entry : fragmentPool.entrySet()) {
			if (entry.getValue() != null) {
				FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
				fragmentTransaction.hide(entry.getValue()).commitAllowingStateLoss();
			}
		}
	}
	
	public void finishFragment() {
		for (Entry<String, Fragment> entry : fragmentPool.entrySet()) {
			if (entry.getValue() != null) {
				entry.getValue().onDestroy();
			}
		}
	}
	
	private void nofityChangeComplete(String key) {
		if (changeCompletes.get(key) != null) {
			List<FragmentContentChangeDaoComplete> array = changeCompletes.get(key);
			for (FragmentContentChangeDaoComplete daoComplete : array) {
				daoComplete.changeCompleteItem(key); 
			}
		}
	}

	public FragmentManager getFragmentManager() {
		return fragmentManager;
	}

	public void setFragmentManager(FragmentManager fragmentManager) {
		this.fragmentManager = fragmentManager;
	}
}
