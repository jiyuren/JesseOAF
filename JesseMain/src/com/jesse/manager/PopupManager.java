package com.jesse.manager;

import com.jesse.custom.component.CustomDialog;
import com.jesse.dao.ButtonDao;
import com.jesse.main.R;
import com.jesse.util.ViewUtil;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.widget.ImageView;

/**
 * @author jeissie 2014/5/27
 */
public class PopupManager {
	
	private static Context mContext;
	
	private static PopupManager popupManager = null;
	
	private int titleBackgroundColor;
	
	private PopupManager() {
		titleBackgroundColor = mContext.getResources().getColor(R.color.green);
	}
	
	public void setTitleBackgroundColor(int titleBackgroundColor) {
		this.titleBackgroundColor = titleBackgroundColor;
	}

	public static PopupManager getInstance(Context context) {
		mContext = context;
		if (popupManager == null) {
			popupManager = new PopupManager();
		}
		return popupManager;
	}
	
	public void release() {
		popupManager = null;
	}
	
	public CustomDialog.Builder popupWithButton(Context context, String title, String content, String buttonName, final ButtonDao button) {
		return popupWithTwoButton(context, title, content, true, ViewUtil.getContext().getResources().getString(R.string.confirm), button, 
				false, ViewUtil.getContext().getResources().getString(R.string.cancel), null);
	}
	
	public CustomDialog.Builder popupWithTwoButton(Context context, String title, String contentText, String positiveButtonName, final ButtonDao positiveButton,
			String negativeButtonName, final  ButtonDao negativeButton) {
		return popupWithTwoButton(context, title, contentText, true, positiveButtonName, positiveButton, true, negativeButtonName, negativeButton);
	}
	
	public CustomDialog.Builder popupWithImageAndButton(Context context, int rId, String title, final ButtonDao button) {
		return popupWithImageAndTwoButton(context, rId, title, true, button, false, null);
	}
	
	public CustomDialog.Builder popupWithImageAndButtonAutoClose(Context context, int rId, String title, final ButtonDao button, float time) {
		autoClose(time);
		return popupWithImageAndTwoButton(context, rId, title, true, button, false, null);
	}
	
	public CustomDialog.Builder popupWithImageAndTwoButton(Context context, int rId, String title, final ButtonDao PositiveButton, final  ButtonDao NegativeButton) {
		return popupWithImageAndTwoButton(context, rId, title, true, PositiveButton, true, NegativeButton);
	}
	
	
	private CustomDialog.Builder popupWithImageAndTwoButton(Context context, int rId, String title, Boolean positiveButton ,final ButtonDao positiveButtonDao, 
			Boolean negativeButton ,final  ButtonDao negativeButtonDao) {
		CustomDialog.Builder builder = new CustomDialog.Builder(context);
		ImageView imageView = new ImageView(context);
		imageView.setImageResource(rId);
		builder.setContentView(imageView);
		builder.setTitle(title);
		builder.setTitleBackgroundColor(titleBackgroundColor);
		if (positiveButton) {
			builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					if (positiveButtonDao != null) {
						positiveButtonDao.onClick();
	            	}
					ViewUtil.closePopup();
				}
			});
		}
		
		if (negativeButton) {
			builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					if (negativeButtonDao != null) {
						negativeButtonDao.onClick();
	            	}
					ViewUtil.closePopup();
				}
			});
		}
		return builder;
	}

	private CustomDialog.Builder  popupWithTwoButton(Context context, String title, String contentText, Boolean positiveButton, String positiveButtonName, final ButtonDao positiveButtonDao,
			Boolean negativeButton, String negativeButtonName,final  ButtonDao negativeButtonDao) {
		CustomDialog.Builder builder = new CustomDialog.Builder(context);
		builder.setMessage(contentText);
		builder.setTitle(title);
		builder.setTitleBackgroundColor(titleBackgroundColor);
		if (positiveButton) {
			builder.setPositiveButton(positiveButtonName, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					if (positiveButtonDao != null) {
						positiveButtonDao.onClick();
	            	}
					ViewUtil.closePopup();
				}
			});
		}
		if (negativeButton) {
			builder.setNegativeButton(negativeButtonName, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					if (negativeButtonDao != null) {
						negativeButtonDao.onClick();
	            	}
					ViewUtil.closePopup();
				}
			});
		}
		return builder;
	}
	
	public void autoClose(float time) {
		Handler x = new Handler();
		x.postDelayed(new Runnable() {
			public void run() {
				ViewUtil.closePopup();
			}
		}, (long) (time * 1000));
	}
}
