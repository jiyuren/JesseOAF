package com.jesse.custom.verticalviewpager;

import android.content.Context;
import android.view.animation.Interpolator;
import android.widget.Scroller;

/**
 * 速度修正
 * 
 * @author jesse
 *
 */
public class FixedSpeedScroller extends Scroller {

	/**
	 * 过度动画持续时间 (单位毫秒)
	 */
	private int mDuration = 750;

	public int getmDuration() {
		return mDuration;
	}

	public void setmDuration(int mDuration) {
		this.mDuration = mDuration;
	}

	public FixedSpeedScroller(Context context) {
		super(context);
	}

	public FixedSpeedScroller(Context context, Interpolator interpolator) {

		super(context, interpolator);

	}

	public FixedSpeedScroller(Context context, Interpolator interpolator,
			boolean flywheel) {

		super(context, interpolator, flywheel);

	}

	@Override
	public void startScroll(int startX, int startY, int dx, int dy, int duration) {

		// Ignore received duration, use fixed one instead

		super.startScroll(startX, startY, dx, dy, mDuration);

	}

	@Override
	public void startScroll(int startX, int startY, int dx, int dy) {

		// Ignore received duration, use fixed one instead

		super.startScroll(startX, startY, dx, dy, mDuration);
	}
}
