package com.jesse.custom.asyncimageview;

public abstract class AsyncImagePathDefine {
	public String iconFilePath;
	public String downloadPath;
	
	public  AsyncImagePathDefine() {
		setIconFilePath();
		setDownloadPath();
	}
	
	public abstract void setIconFilePath();
	public abstract void setDownloadPath();
	
	public String getIconFilePath() {
		return iconFilePath;
	}
	public String getDownloadPath() {
		return downloadPath;
	}
	
	
}
