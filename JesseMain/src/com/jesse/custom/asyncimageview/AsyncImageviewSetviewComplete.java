package com.jesse.custom.asyncimageview;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;

/**
 * AsyncImageview读取图片进度回调
 * 
 * @author jesse
 *
 */
public interface AsyncImageviewSetviewComplete {
	/**
	 * 读取失败，由于url或者网络原因无法完成加载。返回错误标志Drawable
	 * @param drawable
	 */
	void onError(Drawable drawable, Object msg);
	
	/**
	 * 准备中，在读取本地，内存或者下载开始前，首次调用。
	 * @param drawable
	 * @param msg
	 */
	void onPrepare(Drawable drawable, Object msg);
	/**
	 * 读取中，返回读取中的Imageview， 与网络下载百分比
	 * @param drawable
	 * @param percent
	 */
	void onLoading(Drawable drawable, int percent, Object msg);
	
	/**
	 * 读取成功，返回下载的Bitmap，已经bitmap的宽高
	 * @param bitmap
	 * @param width
	 * @param height
	 */
	void onComplete(Bitmap bitmap, int width, int height, Object msg);
}
