package com.jesse.custom.asyncimageview;

import android.graphics.Bitmap;

/**
 * 异步图片下载回调接口
 * @author jesse
 *
 */
public interface AsynImageDownloadDao {
	public void imageLoaded(Bitmap imageBitmap, String imageUrl, Object msg);
}
