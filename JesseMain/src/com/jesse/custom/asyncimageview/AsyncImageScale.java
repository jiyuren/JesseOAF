package com.jesse.custom.asyncimageview;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Message;

public class AsyncImageScale {

	private int width, hight;
	private Bitmap bitmap;
	private String iconKey;
	
	public AsyncImageScale(String iconKey, Bitmap bmp, int width, int hight) {
		this.iconKey = iconKey;
		this.width = width;
		this.hight = hight;
		this.bitmap = bmp;
	}
	
	public AsyncImageScale(String iconKey, Drawable drawable, int width, int hight) {
		this.iconKey = iconKey;
		this.width = width;
		this.hight = hight;
		this.bitmap = ((BitmapDrawable)drawable).getBitmap();
	}

	public AsyncImageScale(Context context, String iconKey, int id, int width, int hight) {
		this.iconKey = iconKey;
		this.width = width;
		this.hight = hight;
		Drawable drawable = context.getResources().getDrawable(id);
		this.bitmap = ((BitmapDrawable)drawable).getBitmap();
	}
	
	public Bitmap scaleToBitmap() {
		return getBitmap();
	}
	
	public Drawable scaleToDrawable() {
		return new BitmapDrawable(getBitmap());
	}
	
	private Bitmap getBitmap(){
		if (bitmap == null) {
			return null;
		}
		if (width == 0 && hight != 0) {
			return getBitmapByHight(iconKey, bitmap, hight);
		}
		if (hight == 0 && width != 0) {
			return getBitmapByWidth(iconKey, bitmap, width);
		}
		if (width != 0 && hight != 0){
            return getScaleBitmap(iconKey, bitmap, width, hight);
		}
		if (width == 0 && hight == 0){
            return getScaleBitmap(iconKey, bitmap, bitmap.getWidth(), bitmap.getHeight());
		}
		return null;
	}
	
	private Bitmap getBitmapByWidth(String iconKey, Bitmap bmp, int width){
		int mImgHeitht;
		int mImgWidth;
		if (bmp == null) return null;
		mImgWidth = bmp.getWidth();
		mImgHeitht = bmp.getHeight();
		float scaleWidth = ((float)width) / mImgWidth;
		float scaleHeight = scaleWidth;
		return scaleBitmap(iconKey, bmp, mImgWidth, mImgHeitht, scaleWidth, scaleHeight);
	}
	
	private Bitmap getBitmapByHight(String iconKey, Bitmap bmp, int hight) {
		int mImgHeitht;
		int mImgWidth;
		if (bmp == null) return null;
		mImgWidth = bmp.getWidth();
		mImgHeitht = bmp.getHeight();
		float scaleHeight = ((float)hight) / mImgHeitht;
		float scaleWidth = scaleHeight;
		return scaleBitmap(iconKey, bmp, mImgWidth, mImgHeitht, scaleWidth, scaleHeight);
	}
	
	private Bitmap getScaleBitmap(String iconKey, Bitmap bmp, int width, int hight) {
		int mImgHeitht;
		int mImgWidth;
		if (bmp == null) return null;
		mImgWidth = bmp.getWidth();
		mImgHeitht = bmp.getHeight();
		float scaleWidth = ((float)width) / mImgWidth;
		float scaleHeight = ((float)hight) / mImgHeitht;
		return scaleBitmap(iconKey, bmp, mImgWidth, mImgHeitht, scaleWidth, scaleHeight);
	}
	
	private Bitmap scaleBitmap(String iconKey, Bitmap bmp, int width, int hight, float scaleWidth, float scaleHeight) {
//		Matrix matrix = new Matrix();
//		matrix.postScale(scaleWidth, scaleHeight);
//		bmp= Bitmap.createBitmap(bmp, 0, 0, width, hight, matrix,true);
//        AsyncImageLoader.getInstance().addBitmapToMemoryCache(iconKey, bmp);
        return bmp;
	}
}
