package com.jesse.custom.component;

import java.util.HashMap;
import java.util.Map;

import com.jesse.util.ViewUtil;

import android.view.View;

public class ViewHolder {
	
	private ViewHolder () {}
	
	@SuppressWarnings("unchecked")
	public static <T extends View> T get(View view, int id) {
		 Map<Integer, View> viewHolder = (Map<Integer, View>) view.getTag();
		 if (viewHolder == null) {
			 viewHolder = new HashMap<Integer, View>();
			 view.setTag(viewHolder);
		 }
		 View childView = viewHolder.get(id);
		 if (childView == null) {
			 childView = view.findViewById(id);
			 if (childView == null) {
				 ViewUtil.Debug("view");
			 }
	         viewHolder.put(id, childView);
		 }
	     return (T) childView;
	 }
}
