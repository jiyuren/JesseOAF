package com.jesse.custom.component;

import com.jesse.main.R;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * 多功能按钮
 * @author jesse
 */
public class SuperButton extends LinearLayout implements OnTouchListener{

	private RelativeLayout bgLayout;
	private TextView buttonText;
	private ImageView imageBg;
	private boolean superButton = false;
	private boolean focusBgChange = false;
	private boolean focusText = false;
	private boolean zoomButton = false;
	private boolean touchEnable = true;
	private int normalBgColor = 0;
	private int focusBgColor = 0;
	private int normalTextColor = 0;
	private int focusTextColor = 0;
	private int normalResId = 0;
	private int focusResId = 0;
	
	public SuperButton(Context context) {
		super(context);
	}

	public SuperButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View layout = inflater.inflate(R.layout.unit_button, this);
		bgLayout = (RelativeLayout) layout.findViewById(R.id.buttonContenner);
		buttonText = (TextView) layout.findViewById(R.id.buttonContennerText);
		imageBg = (ImageView) layout.findViewById(R.id.imageButtonBg);
	}

	//焦点切换的图片按钮
	public void initSuperBbutton(int normalResId, int focusResId, int bgColor) {
		superButton = true;
		buttonText.setVisibility(View.GONE);
		this.normalResId = normalResId;
		this.focusResId = focusResId;
		imageBg.setImageBitmap(BitmapFactory.decodeResource(getResources(), normalResId));
		setButtonBackgroundColor(getResources().getColor(bgColor));
	}
	
	//缩放的图片按钮
	public void initZoomButton(int resId) {
		initZoomButton(resId, "");
	}
	public void initZoomButton(int resId, int bgColor) {
		initZoomButton(resId,"");
		setButtonBackgroundColor(getResources().getColor(bgColor));
	}
	public void initZoomButton(int resId, String text) {
		zoomButton = true;
		setButtonText(text);
		setButtonTextDefultColor();
		imageBg.setImageBitmap(BitmapFactory.decodeResource(getResources(), resId));
	}
	public void initZoomButton(Drawable drawable) {
		initZoomButton(drawable, "");
	}
	public void initZoomButton(Drawable drawable, String text) {
		zoomButton = true;
		setButtonText(text);
		setButtonTextDefultColor();
		imageBg.setImageDrawable(drawable);
		setButtonBackgroundColor(getResources().getColor(android.R.color.transparent));
	}
	
	//带底色的文字按钮
	public void initButton(String buttonString) {
		initButton(buttonString, R.color.white);
	}
	
	public void initButton(String buttonString, int textColor) {
		initButton(buttonString, textColor, R.color.btn_green_normal, R.color.btn_green_down);
		focusBgChange(R.color.btn_green_normal, R.color.btn_green_down);
	}
	
    public void initButton(int textId, int textColor, int normalColor, int focusColor) {
        initButton(getResources().getString(textId), textColor, normalColor, focusColor);
    }
    
    public void initButton(String buttonString, int textColor, int normalColor, int focusColor) {
        setButtonText(buttonString);
        buttonText.setTextColor(getResources().getColor(textColor));
        setButtonBackgroundColor(getResources().getColor(normalColor));
        focusBgChange(normalColor, focusColor);
    }

	//纯文字按钮
	public void initTextButton(String buttonString, int textColor, int normalColor, int focusColor) {
		setButtonText(buttonString);
		buttonText.setTextColor(getResources().getColor(textColor));
		setButtonBackgroundColor(getResources().getColor(android.R.color.transparent));
		focusTextChange(normalColor, focusColor);
	}
	
	public void setTextSize(float size) {
		buttonText.setTextSize(size);
	}
	
	public void setTextColor(int colorId) {
		buttonText.setTextColor(colorId);
	}
	
	public void setButtonText(String text) {
		buttonText.setText(text);
	}
	
	public void setButtonTextDefultColor() {
		buttonText.setTextColor(getResources().getColor(R.color.white));	
	}
	
	//是否开启按钮缩放功能
	public boolean isZoomButton() {
		return zoomButton;
	}

	public void setZoomButton(boolean zoomButton) {
		this.zoomButton = zoomButton;
	}
		
	public void setButtonBackgroundColor(int color) {
		bgLayout.setBackgroundColor(color);
	}
	
	public boolean isTouchEnable() {
		return touchEnable;
	}

	public void setTouchEnable(boolean touchEnable) {
		setEnabled(touchEnable);
		this.touchEnable = touchEnable;
	}

	public void recycleImageView() {
		if (imageBg.getDrawable() != null && imageBg.getDrawable() instanceof BitmapDrawable){
			Bitmap oldBitmap = ((BitmapDrawable)imageBg.getDrawable()).getBitmap();
			if (oldBitmap != null && !oldBitmap.isRecycled()) {
				oldBitmap.recycle();
				oldBitmap = null;
			}
		}
	}
	
	private void focusBgChange(int normalBgColor, int focusBgColor) {
		focusBgChange = true;
		focusText = false;
		this.normalBgColor = normalBgColor;
		this.focusBgColor = focusBgColor;
	}
	
	private void focusTextChange(int normalTextColor, int focusTextColor) {
		focusBgChange = false;
		focusText = true;
		this.normalTextColor = normalTextColor;
		this.focusTextColor = focusTextColor;
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		super.onTouchEvent(event);
		if (!touchEnable) {
			return true;
		}
		if(event.getAction() == MotionEvent.ACTION_DOWN){ 
			if (superButton) {
				imageBg.setImageBitmap(BitmapFactory.decodeResource(getResources(), focusResId));
			}
			if (focusBgChange) {
				setButtonBackgroundColor(getResources().getColor(focusBgColor));
			}
			if (focusText) {
				buttonText.setTextColor(getResources().getColor(focusTextColor));
			}
			if (zoomButton) {
				bgLayout.setScaleX(1.03f);
				bgLayout.setScaleY(1.03f);
			}
			
        }   
        else if(event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL){  
        	if (superButton) {
				imageBg.setImageBitmap(BitmapFactory.decodeResource(getResources(), normalResId));
			}
        	if (focusBgChange) {
        		setButtonBackgroundColor(getResources().getColor(normalBgColor));
        	} 
        	if (focusText) {
        		buttonText.setTextColor(getResources().getColor(normalTextColor));
        	}
        	if (zoomButton) {
				bgLayout.setScaleX(1.0f);
				bgLayout.setScaleY(1.0f);
			}
        } 
		return true;
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		return false;
	}
	
	public void onClick() {
		onClick();
	}
}
