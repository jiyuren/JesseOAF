package com.jesse.custom.component;

import com.jesse.main.R;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * @author jeissie 2014/5/27
 */
public class CustomDialog extends Dialog {

	public CustomDialog(Context context) {
		super(context);
	}

	public CustomDialog(Context context, int theme) {
		super(context, theme);
	}

	public static class Builder {
		private Context context;
		private int titleBackgroundColor;
		private String title;
		private String message;
		private String positiveButtonText;
		private String negativeButtonText;
		private View contentView;
		private OnClickListener positiveButtonClickListener;
		private OnClickListener negativeButtonClickListener;
		private Boolean canBeCancle = false;
		private CustomDialog dialog;
		public Builder(Context context) {
			this.context = context;
		}

		public Builder setMessage(String message) {
			this.message = message;
			return this;
		}

		public Builder setMessage(int message) {
			this.message = (String) context.getText(message);
			return this;
		}

		public Builder setTitleBackgroundColor(int titleBackgroundColor) {
			this.titleBackgroundColor = titleBackgroundColor;
			return this;
		}
		
		public Builder setTitle(int title) {
			this.title = (String) context.getText(title);
			return this;
		}

		public Builder setTitle(String title) {
			this.title = title;
			return this;
		}

		public Builder setContentView(View v) {
			this.contentView = v;
			return this;
		}

		public void setCancelable(boolean cancle) {
			this.canBeCancle = cancle;
		}

		public Builder setPositiveButton(int positiveButtonText,
				OnClickListener listener) {
			this.positiveButtonText = (String) context
					.getText(positiveButtonText);
			this.positiveButtonClickListener = listener;
			return this;
		}

		public Builder setPositiveButton(String positiveButtonText,
				OnClickListener listener) {
			this.positiveButtonText = positiveButtonText;
			this.positiveButtonClickListener = listener;
			return this;
		}

		public Builder setNegativeButton(int negativeButtonText,
				OnClickListener listener) {
			this.negativeButtonText = (String) context
					.getText(negativeButtonText);
			this.negativeButtonClickListener = listener;
			return this;
		}

		public Builder setNegativeButton(String negativeButtonText,
				OnClickListener listener) {
			this.negativeButtonText = negativeButtonText;
			this.negativeButtonClickListener = listener;
			return this;
		}

		public CustomDialog create() {
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			dialog = new CustomDialog(context,R.style.Dialog);
			View layout = inflater.inflate(R.layout.unit_dialog, null);
			dialog.addContentView(layout, new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
			TextView titleTextView = (TextView) layout.findViewById(R.id.title);
			titleTextView.setText(title);
			titleTextView.setBackgroundColor(titleBackgroundColor);
			if (positiveButtonText != null) {
				((SuperButton) layout.findViewById(R.id.positiveButton)).initButton(positiveButtonText, R.color.text_gray, R.color.white, R.color.gray);
				if (positiveButtonClickListener != null) {
					((SuperButton) layout.findViewById(R.id.positiveButton))
							.setOnClickListener(new View.OnClickListener() {
								public void onClick(View v) {
									positiveButtonClickListener.onClick(dialog,
											DialogInterface.BUTTON_POSITIVE);
								}
							});
				}
			} else {
				layout.findViewById(R.id.positiveButton).setVisibility(View.GONE);
			}
			if (negativeButtonText != null) {
				((SuperButton) layout.findViewById(R.id.negativeButton)).initButton(negativeButtonText, R.color.text_gray, R.color.white, R.color.gray);
				if (negativeButtonClickListener != null) {
					((SuperButton) layout.findViewById(R.id.negativeButton))
							.setOnClickListener(new View.OnClickListener() {
								public void onClick(View v) {
									negativeButtonClickListener.onClick(dialog,
											DialogInterface.BUTTON_NEGATIVE);
								}
							});
				}
			} else {
				layout.findViewById(R.id.negativeButton).setVisibility(View.GONE);
			}
			if (message != null) {
				TextView messageTextView = (TextView) layout.findViewById(R.id.message);
				messageTextView.setText(message);
				messageTextView.setMovementMethod(ScrollingMovementMethod.getInstance());
			} else if (contentView != null) {
				((LinearLayout) layout.findViewById(R.id.messageContent)).removeAllViews();
				((LinearLayout) layout.findViewById(R.id.messageContent)).addView(
						contentView, new LayoutParams(
								LayoutParams.WRAP_CONTENT,
								LayoutParams.WRAP_CONTENT));
			}
			dialog.setContentView(layout);
			dialog.setCancelable(canBeCancle);
			return dialog;
		}

	}
}
