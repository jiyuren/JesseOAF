package com.jesse.custom.component;

import com.jesse.custom.component.SuperButton;
import com.jesse.dao.ButtonDao;
import com.jesse.main.R;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

public class EmptyAlertStub extends LinearLayout{

	private TextView alertTextView;
	private ProgressBar progressBar;
	private SuperButton emptyButton;
	private ButtonDao buttonDao;
	
	public EmptyAlertStub(Context context) {
		super(context);
	}

	public EmptyAlertStub(Context context, AttributeSet attrs) {
		super(context, attrs);
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View layout = inflater.inflate(R.layout.unit_empty_alert, this);
		alertTextView = (TextView) layout.findViewById(R.id.emptyAlertText);
		progressBar = (ProgressBar) layout.findViewById(R.id.emptyAlertBar);
		emptyButton = (SuperButton) layout.findViewById(R.id.emptyButton);
		hide();
	}
	
	//隐藏文字与按钮
	public void hide() {
		hideWating();
		hideButton();
	}
	
	//文字占位符
	public void initText(String alertText) {
		alertTextView.setText(alertText);
	}
	
	public void showText(String alertText) {
		showTextView();
		alertTextView.setText(alertText);
	}
	
	//进度占位符
	public void showWating(String text) {
		showText(text);
		progressBar.setVisibility(View.VISIBLE);
	}
	
	//隐藏文字与进度
	public void hideWating() {
		hideTextView();
		progressBar.setVisibility(View.GONE);
	}
	
	public void initLogo(Drawable btnDrawable) {
		emptyButton.initZoomButton(btnDrawable);
		emptyButton.setZoomButton(false);
		hide();
	}
	
	public void showLogo() {
		showButton();
	}
	
	//等待按钮占位符
	public void initButton(Drawable btnDrawable, final ButtonDao buttonDao) {
		emptyButton.initZoomButton(btnDrawable);
		emptyButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (buttonDao != null) {
					buttonDao.onClick();
				}
			}
		});
		hide();
	}
	
	public void showButton(Drawable btnDrawable, final SuperButton buttonDao) {
		hide();
		emptyButton.initZoomButton(btnDrawable);
		emptyButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				buttonDao.onClick();
			}
		});
		emptyButton.setVisibility(View.VISIBLE);
	}
	
	//隐藏按钮
	public void showButton() {
		emptyButton.setVisibility(View.VISIBLE);
	}
	
	public void hideButton() {
		emptyButton.setVisibility(View.GONE);
	}
	
	private void showTextView() {
		progressBar.setVisibility(View.GONE);
		alertTextView.setVisibility(View.VISIBLE);
		hideButton();
	}
	
	private void hideTextView() {
		progressBar.setVisibility(View.GONE);
		alertTextView.setVisibility(View.GONE);
		hideButton();
	}
}
