package com.jesse.custom.component;

import com.jesse.main.R;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

public class SlipSwitchButton extends View {
	private float lastX;
	private float lastY;
    private boolean NowChoose = false;// 记录当前按钮是否打开,true为打开,flase为关闭
    private boolean isChecked;
    private boolean OnSlip = false;	// 记录用户是否在滑动的变量
    private float DownX, NowX;// 按下时的x,当前的x
    private Rect Btn_On, Btn_Off;// 打开和关闭状态下,游标的Rect .
    private boolean isChgLsnOn = false;
    private OnChangedListener ChgLsn;
    private Bitmap bg_on, bg_off, slip_btn;

    public SlipSwitchButton(Context context)
    {
        super(context);
        init();
    }

    public SlipSwitchButton(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init();
    }

    public SlipSwitchButton(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        init();
    }

    private void init()
    {

        bg_on = BitmapFactory.decodeResource(getResources(), R.drawable.switch_on);
        bg_off = BitmapFactory.decodeResource(getResources(), R.drawable.switch_off);
        slip_btn = BitmapFactory.decodeResource(getResources(), R.drawable.switch_button);
        Btn_On = new Rect(0, 0, slip_btn.getWidth(), slip_btn.getHeight());
        Btn_Off = new Rect(bg_off.getWidth() - slip_btn.getWidth(), 0, bg_off.getWidth(),
                slip_btn.getHeight());
    }

    @Override
    protected void onDraw(Canvas canvas)
    {

        super.onDraw(canvas);

        Matrix matrix = new Matrix();
        Paint paint = new Paint();
        float x;

        if (NowX < (bg_on.getWidth() / 2))// 滑动到前半段与后半段的背景不同,在此做判断
        {
            x = NowX - slip_btn.getWidth() / 2;
            canvas.drawBitmap(bg_off, matrix, paint);// 画出关闭时的背景
        }

        else
        {
            x = bg_on.getWidth() - slip_btn.getWidth() / 2;
            canvas.drawBitmap(bg_on, matrix, paint);// 画出打开时的背景
        }

        if (OnSlip)// 是否是在滑动状态,

        {
            if (NowX >= bg_on.getWidth())// 是否划出指定范围,不能让游标跑到外头,必须做这个判断

                x = bg_on.getWidth() - slip_btn.getWidth() / 2;// 减去游标1/2的长度...

            else if (NowX < 0)
            {
                x = 0;
            }
            else
            {
                x = NowX - slip_btn.getWidth() / 2;
            }
        } else
        {// 非滑动状态

            if (NowChoose)// 根据现在的开关状态设置画游标的位置
            {
                x = Btn_Off.left;
                canvas.drawBitmap(bg_on, matrix, paint);// 初始状态为true时应该画出打开状态图片
            }
            else
                x = Btn_On.left;
        }
        if (isChecked)
        {
            canvas.drawBitmap(bg_on, matrix, paint);
            x = Btn_Off.left;
            isChecked = !isChecked;
        }

        if (x < 0) {
        	x = 0;
        } else if (x > bg_on.getWidth() - slip_btn.getWidth()) {
        	x = bg_on.getWidth() - slip_btn.getWidth();
        }
        canvas.drawBitmap(slip_btn, x, 0, paint);// 画出游标.

    }
    
    public boolean onTouchEvent(MotionEvent event) {
    	int minClickDis = 20;
        switch (event.getAction()) {
        
	        case MotionEvent.ACTION_DOWN:
	        	getParent().requestDisallowInterceptTouchEvent(true);
	            if(event.getX()>bg_on.getWidth()||event.getY()>bg_on.getHeight())  
	                return false;  
                OnSlip = true;  
                DownX = event.getX();  
                NowX = DownX;  
                break; 
	                
	        case MotionEvent.ACTION_MOVE://滑动  
	        	getParent().requestDisallowInterceptTouchEvent(true);
	            NowX = event.getX();  
	            break;  
                
            case MotionEvent.ACTION_UP:
            	getParent().requestDisallowInterceptTouchEvent(false);
            	boolean oldNowChoose = NowChoose;
            	if (DownX != NowX) {
	                if (NowX >= (bg_on.getWidth() / 2)) {
	                    NowX = bg_on.getWidth() - slip_btn.getWidth() / 2;
	                    NowChoose = true;
	                }
	                else {
	                    NowX = NowX - slip_btn.getWidth() / 2;
	                    NowChoose = false;
	                }
            	} else {
            		NowChoose = !NowChoose;
            		if (NowChoose) {
            			NowX = bg_on.getWidth() - slip_btn.getWidth() / 2;
            		} else {
            			NowX = NowX - slip_btn.getWidth() / 2;
            		}
            	}
                if (isChgLsnOn && oldNowChoose != NowChoose) {
                	ChgLsn.OnChanged(NowChoose);
                }
                OnSlip = false;
                break;
        }
        invalidate();
        return true;
    }
    
    public void setOnChangedListener(OnChangedListener l)
    {
        isChgLsnOn = true;
        ChgLsn = l;
    }

    public interface OnChangedListener
    {
        abstract void OnChanged(boolean CheckState);
    }

    public void setChecked(boolean isChecked)
    {
        this.isChecked = isChecked;
        NowChoose = isChecked;
        invalidate();
    }
    
    public boolean isChecked() {
    	return NowChoose;
    }
}
