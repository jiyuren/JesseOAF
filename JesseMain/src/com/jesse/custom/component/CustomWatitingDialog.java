package com.jesse.custom.component;

import com.jesse.main.R;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

public class CustomWatitingDialog extends ProgressDialog  {
	private TextView alertText;
	private String alertString = "";
	public CustomWatitingDialog(Context context) {
		super(context);
	}
	
	public CustomWatitingDialog(Context context, String message){  
	        super(context);  
	        this.alertString = message;  
    }
	
	public CustomWatitingDialog(Context context, String message, boolean cancelable){  
        super(context);  
        this.alertString = message;  
        this.setCancelable(cancelable);
		if (cancelable) {
			this.setOnCancelListener(new OnCancelListener() {
				
				@Override
				public void onCancel(DialogInterface dialog) {
					//TODO fix observer
//					ObserverManager.getInstance().cancelAllNetServer(ServerBase.CANCEL);
				}
			});
		}
	}

	@Override  
    protected void onCreate(Bundle savedInstanceState) {  
        super.onCreate(savedInstanceState);  
        setContentView(R.layout.unit_waiting_dialog);  
        alertText = (TextView) findViewById(R.id.alertText);  
        alertText.setText(alertString);  
    } 
	
	@Override
	public void dismiss() {
		super.dismiss();
	}
}
