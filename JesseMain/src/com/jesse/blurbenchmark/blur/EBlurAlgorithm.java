package com.jesse.blurbenchmark.blur;

import java.util.ArrayList;
import java.util.List;

import com.jesse.main.R;

/**
 * Enum of all supported algorithms
 *
 * @author pfavre
 */
public enum EBlurAlgorithm {
    RS_GAUSS_FAST(R.color.red), RS_BOX_5x5(R.color.blue),
	RS_GAUSS_5x5(R.color.pink), RS_STACKBLUR(R.color.darkBlue),
//    NDK_STACKBLUR(R.color.graphBgOrange),
//    NDK_NE10_BOX_BLUR(R.color.graphBgSkyBlue),
    STACKBLUR(R.color.black),
	GAUSS_FAST(R.color.btn_gray_normal), BOX_BLUR(R.color.btn_orange), NONE(R.color.btn_gray)
    ;

	/**
	 * Color used in graphs
	 */
	private final int colorResId;

	EBlurAlgorithm(int colorResId) {
		this.colorResId = colorResId;
	}

	public int getColorResId() {
		return colorResId;
	}

	public static List<EBlurAlgorithm> getAllAlgorithms() {
        List<EBlurAlgorithm> algorithms = new ArrayList<EBlurAlgorithm>();
        for (EBlurAlgorithm algorithm : values()) {
            if(!algorithm.equals(NONE)) {
                algorithms.add(algorithm);
            }
        }
        return algorithms;
    }
}
