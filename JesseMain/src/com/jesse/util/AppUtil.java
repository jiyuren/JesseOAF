package com.jesse.util;

import java.util.List;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.ActivityManager.RunningTaskInfo;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Bitmap.Config;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

public class AppUtil {

	public static boolean DEBUG = false;
	
	public static boolean netStateOk = true;
	
	/**
	 * 是否进入后台之后，又返回前台
	 */
	public static boolean isBackInForeground = false;
	
	
	public static boolean isBackInForeground() {
		return isBackInForeground;
	}

	public static void setBackInForeground(boolean isBackInForeground) {
		AppUtil.isBackInForeground = isBackInForeground;
	}

	public static boolean isAppOnForeground(Context context, String packageName) { 
    	ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE); 
        // Returns a list of application processes that are running on the device 
	    List<RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses(); 
	    if (appProcesses == null) return false; 
	    
	    for (RunningAppProcessInfo appProcess : appProcesses) { 
//	    	ViewUtil.Debug(appProcess.processName);
	        // The name of the process that this object is associated with. 
	        if (appProcess.processName.equals(packageName) 
	                && appProcess.importance == RunningAppProcessInfo.IMPORTANCE_FOREGROUND) { 
	            return true; 
	        } 
	    } 
	    return false; 
	}
	
	public static String getTopActivityName(Context context){
		 String topActivityClassName = null;
		 ActivityManager activityManager = (ActivityManager)(context.getSystemService(android.content.Context.ACTIVITY_SERVICE )) ;
		 //即最多取得的运行中的任务信息(RunningTaskInfo)数量
	     List<RunningTaskInfo> runningTaskInfos = activityManager.getRunningTasks(1) ;
	     if(runningTaskInfos != null){
	    	 ComponentName f = runningTaskInfos.get(0).topActivity;
	    	 topActivityClassName = f.getClassName();
	    	
	     }
	     return topActivityClassName;
	}
	
	/**
	 * 获取版本号 整数值
	 * @param context
	 * @return
	 */
	public static String getVersionCode(Context context) {
		 PackageManager packageManager = context.getPackageManager();
        PackageInfo packInfo = null;
		try {
			packInfo = packageManager.getPackageInfo(context.getPackageName(),0);
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
        String version = packInfo.versionCode + "";
        return version;
	}
	
	/**
	 * 获取版本名称  eg:x.x.x.x
	 * @param context
	 * @return
	 */
	public static String getVersionName(Context context) {
		 PackageManager packageManager = context.getPackageManager();
       PackageInfo packInfo = null;
		try {
			packInfo = packageManager.getPackageInfo(context.getPackageName(),0);
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
       return packInfo.versionName;
	}
	
	/**
	 * 根据包名获取App名称
	 * 
	 * @param context
	 * @param packageName
	 * @return
	 */
	public static String getApplicationName(Context context, String packageName) { 
       PackageManager packageManager = null; 
       ApplicationInfo applicationInfo = null; 
       try { 
           packageManager = context.getApplicationContext().getPackageManager(); 
           applicationInfo = packageManager.getApplicationInfo(packageName, 0); 
           if (applicationInfo == null) {
           	return packageName;
           }
           String applicationName = (String) packageManager.getApplicationLabel(applicationInfo); 
           return applicationName; 
       } catch (PackageManager.NameNotFoundException e) { 
           applicationInfo = null; 
       }
		return packageName; 
	} 
	
	/**
	 * 根据包名获取App 图标
	 * @param context
	 * @param packageName
	 * @return
	 */
	public static Drawable getApplicationIcon(Context context, String packageName) { 
       PackageManager packageManager = null; 
       ApplicationInfo applicationInfo = null; 
       try { 
           packageManager = context.getApplicationContext().getPackageManager(); 
           applicationInfo = packageManager.getApplicationInfo(packageName, 0); 
           if (applicationInfo == null) {
           	ViewUtil.Debug("applicationInfo 是空的");
           	return null;
           }
           return packageManager.getApplicationIcon(packageName);
       } catch (PackageManager.NameNotFoundException e) { 
           applicationInfo = null; 
       }
		return null; 
   } 
	
	/**
     * Scrollview与ListView嵌套时，重新计算listview高度
     * @param listView
     */
  	public static void resetListViewHeight(ListView listView) {  
          ListAdapter listAdapter = listView.getAdapter();   
          if (listAdapter == null) {  
              return;  
          }  
    
          int totalHeight = 0;  
          for (int i = 0; i < listAdapter.getCount(); i++) {  
              View listItem = listAdapter.getView(i, null, listView);  
              listItem.measure(0, 0);  
              totalHeight += listItem.getMeasuredHeight();  
          }  
    
          ViewGroup.LayoutParams params = listView.getLayoutParams();  
          params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));  
          listView.setLayoutParams(params);  
    }
  	
  	/**
  	 * 通过View试图生成BitMap
  	 * 
  	 * @param view 任何view或其的子类均可
  	 * @return
  	 */
  	public static Bitmap convertViewToBitmap(View view){
  		 if (view == null) {  
             return null;  
         }  
         Bitmap screenshot;  
         screenshot = Bitmap.createBitmap(view.getWidth(), view.getMeasuredHeight(), Config.RGB_565);
         Canvas c = new Canvas(screenshot);  
         c.translate(-view.getScrollX(), -view.getScrollY()); 
         view.draw(c);  
         return screenshot;  
  	}
}
