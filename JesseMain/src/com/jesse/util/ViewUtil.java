package com.jesse.util;


import android.app.Activity;
import android.app.Application;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;







import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.jesse.base.ManagerBase;
import com.jesse.custom.component.CustomDialog;
import com.jesse.custom.component.CustomWatitingDialog;
import com.jesse.dao.ButtonDao;
import com.jesse.main.R;
import com.jesse.manager.PopupManager;

/**
 * 系统试图控制类，集合了页面跳转，弹窗，Toast提示， log提示功能。
 * 
 * @author jeissie 2014/5/26
 */
public class ViewUtil {
	//当前设备屏幕宽度
	private static int screenWidth = 0;
	//当前设备屏幕高度
	private static int screenHeight = 0;
	//等待弹窗
	private static Dialog waitingDialog = null; 
	//按键弹窗
	private static Dialog popup = null;
	//当前焦点Context
	private static Context context = null;
	//当前AppContext
	private static Application applicationContext = null;
	//当前入栈的Context
	private static List<Context> contexts = new ArrayList<Context>();
	
	/**
	 * {@link ManagerBase}
	 * 
	 * 系统中Manager单例退出释放
	 */
	private static Map<String, ManagerBase> managerList = new HashMap<String, ManagerBase>();
	
	public static Application getApplicationContext() {
		return applicationContext;
	}

	public static void setApplicationContext(Application applicationContext) {
		ViewUtil.applicationContext = applicationContext;
	}

	public static Context getContext() {
		return context;
	}

	public static void setContext(Context context) {
		ViewUtil.context = context;
		contexts.add(context);
	}
	
	public static void setCurrentContext(Context context) {
		ViewUtil.context = context;
	}

	public static List<Context> getContexts() {
		return contexts;
	}
	
	public static void removeContext(Context context) {
		contexts.remove(context);	
	}
	
	public static void exitSystem() {
		clearSystem();
		finishAndClearContext();
		System.exit(0);
	}
	
	public static void finishAndClearContext() {
		int size = contexts.size();
		context = null;
		for (int i = 0; i < size ; i++) {
			Activity itemActivity = (Activity)contexts.get(i);
			if (itemActivity != null) {
				itemActivity.finish();
			}
		}
		contexts.clear();
	}
	
	public static void clearSystem() {
		closePopup();
		releaseAllManager();
	}
	
	public static void addManamger(String tag, ManagerBase manager) {
		managerList.put(tag, manager);
	}
	
	public static void releaseAllManager() {
		 Iterator iter = managerList.entrySet().iterator();
		 while(iter.hasNext()){
			 Map.Entry entry = (Map.Entry)iter.next();
			 ManagerBase item = (ManagerBase) entry.getValue();
			 item.releaseManager();
		 }
	}
	
	public static void exchangeActivity(Object object) {
		closePopup();
		Intent i = new Intent(context, (Class<?>) object);
		context.startActivity(i);
		((Activity) context).overridePendingTransition(R.anim.in_from_right, 0);
	}
	
	public static void exchangeActivity(Intent intent) {
		closePopup();
		context.startActivity(intent);
		((Activity) context).overridePendingTransition(R.anim.in_from_right, 0);
	}
	
	public static void exchangeAndFinishActivity(Intent intent) {
		Activity itemActivity = (Activity)contexts.get(contexts.size() - 1);
		exchangeAndFinishActivity(itemActivity, intent);
	}
	
	public static void exchangeAndFinishActivity(Object object) {
		Activity itemActivity = (Activity)contexts.get(contexts.size() - 1);
		exchangeAndFinishActivity(itemActivity, (Class<?>) object);
	}
	
	public static void exchangeAndFinishActivity(Activity activity, Intent intent) {
		closePopup();
		activity.startActivity(intent);
		((Activity) activity).overridePendingTransition(R.anim.in_from_right, R.anim.out_to_right);
		activity.finish();
	}
	
	public static void exchangeAndFinishActivity(Activity activity, Class startClass) {
		closePopup();
		Intent i = new Intent(activity, startClass);
		activity.startActivity(i);
		((Activity) activity).overridePendingTransition(R.anim.in_from_right, R.anim.out_to_right);
		activity.finish();
	}
	
	public static void exchangeAndFinishAllActivity(Context context, Object object) {
		exitSystem();
		Intent i = new Intent(context, (Class<?>) object);
		context.startActivity(i);
		((Activity) context).overridePendingTransition(R.anim.in_from_right, R.anim.out_to_right);
	}
	
	public static void finishActivityByCount(int count) {
		int size = contexts.size();
		count = count > size ? size : count;
		for (int i = size; i > size - count ; i--) {
			Activity itemActivity = (Activity)contexts.get(i - 1);
			if (itemActivity != null) {
				itemActivity.finish();
			}
		}
	}
	
	public static int getScreenWidth() {
		return screenWidth;
	}

	public static void setScreenWidth(int screenWidth) {
		ViewUtil.screenWidth = screenWidth;
	}
	
	public static int getScreenHeight() {
		return screenHeight;
	}

	public static void setScreenHeight(int screenHeight) {
		ViewUtil.screenHeight = screenHeight;
	}
	
	public static void popup(String content) {
		popopWithButton(content, null);
	}
	
	public static void popopWithButton(String content, ButtonDao button) {
		popupWithButton(content, context.getString(R.string.alert),  context.getString(R.string.confirm), button);
	}
	
	public static void popupWithButton(final String content, final String title, final String buttonName, final ButtonDao button) {
		Handler x = new Handler();
	       x.postDelayed(new Runnable() {
	           public void run() {
	        	   if (popup != null) {
		       			//TODO Popup should be a queue,and one by one be poped to fount.
		       			closePopup();
		       	   }
		       		CustomDialog.Builder builder = PopupManager.getInstance(context).popupWithButton(context, title, content, buttonName, button);
		       		builder.setCancelable(false); 
		       		popup = builder.create();
		       		popup.show();
	           }
       }, 300);
		
	}

	public static void popupWithTwoButton(String contentText, final ButtonDao PositiveButton, final  ButtonDao NegativeButton) {
		popupWithTwoButton(contentText, context.getString(R.string.alert), context.getString(R.string.confirm), PositiveButton, 
				context.getString(R.string.cancel), NegativeButton);
	}
	
	public static void popupWithTwoButton(final String contentText, final String title, final String positiveButtonName, final ButtonDao PositiveButton, 
			final String negativeButtonName, final  ButtonDao NegativeButton) {
		Handler x = new Handler();
	       x.postDelayed(new Runnable() {
	           public void run() {
	        	   if (popup != null) {
		       			//TODO Popup should be a queue,and one by one be poped to fount.
		       			closePopup();
		       		}
		       		CustomDialog.Builder builder = PopupManager.getInstance(context).popupWithTwoButton(context, title, contentText, 
		       				positiveButtonName, PositiveButton, negativeButtonName, NegativeButton);
		       		builder.setCancelable(false); 
		       		popup = builder.create();
		       		popup.show();
	           }
       }, 300);
		
	}
	
	public static void popupSignAlert () {
		if (popup != null) {
			//TODO Popup should be a queue,and one by one be poped to fount.
			closePopup();
		}
		CustomDialog.Builder builder = PopupManager.getInstance(context).popupWithImageAndButtonAutoClose(context, R.drawable.loading_bar, "签到成功", null, 2);
		builder.setCancelable(false); 
		popup = builder.create();
		popup.show();
	}
	
	public static void popupWihtImageAndButton (String title, int rid, ButtonDao button) {
		if (popup != null) {
			//TODO Popup should be a queue,and one by one be poped to fount.
			closePopup();
		}
		CustomDialog.Builder builder = PopupManager.getInstance(context).popupWithImageAndButton(context, rid, title, button);
		builder.setCancelable(false); 
		popup = builder.create();
		popup.show();
	}
	
	public static void closePopup() {
		if (popup == null)  return;
		popup.dismiss();
		popup = null;
	}
	
	public static Dialog getPopup() {
		return popup;
	}

	public static void setPopup(Dialog popup) {
		ViewUtil.popup = popup;
	}

	public static void Toast(int text) {
		Toast(text + "");
	}
	
	public static void Toast(float text) {
		Toast(text + "");
	}
	
	public static void Toast(String text) {
		if (context == null) {
			throw new NullPointerException("The activity context is null");
		}
		Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
	}
	
	public static void message(int text) {
		message("" + text);
	}
	
	public static void message(String text) {
		Log.v("JOAF Message !!!", text);
	}
	
	public static void Error(int code) {
		Error(code + "");
	}
	
	public static void Error(String text) {
		Log.v("JOAF Error !!!", text);
	}
	
	public static void Debug(int code) {
		Debug(code + "");
	}
	
	public static void Debug(String text) {
		if (!AppUtil.DEBUG) return;
		System.out.println("JOAF Debug !!! " + text);
	}
	
	public static void waiting() {
		waiting("");
	}
	
	public static void waiting(String text) {
		waiting(text, false);
	}
	
	public static void waiting(String text, boolean canBeCancel) {
		if (waitingDialog != null) {
			endWaiting();
		}
		if (text.isEmpty()) {
			text = context.getString(R.string.pleaseWaiting);
		}

		waitingDialog = new CustomWatitingDialog(context, text, canBeCancel);
		waitingDialog.show();
	}
	
	public static void waiting(Dialog dialog) {
		if (waitingDialog != null) {
			endWaiting();
		}
		waitingDialog = dialog;
		waitingDialog.show();
	}
	
	public static Dialog getWaiting() {
        return waitingDialog;
	}
	
	public static void endWaiting() {
		if (waitingDialog == null) return;
		waitingDialog.dismiss();
		waitingDialog = null;
	}
	
	public static Dialog getWaitingDialog() {
		return waitingDialog;
	}

	public static void setWaitingDialog(Dialog waitingDialog) {
		ViewUtil.waitingDialog = waitingDialog;
	}
	
	public static void hideKeyBoard() {
		if (context == null) return;
		InputMethodManager inputmanger = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
		inputmanger.hideSoftInputFromWindow(((Activity) context).getWindow().peekDecorView().getWindowToken(), 0);	
	}
}
