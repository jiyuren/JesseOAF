package com.jesse.util;

import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.drawable.Drawable;
import android.graphics.Canvas;
import android.graphics.Color;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

public class MyUtil {

	/**
	 * 加密字符串为MD5值
	 * @param plainText 输入字符串
	 * @return
	 */
	public static String Md5(String plainText ) {
		try { 
			MessageDigest md = MessageDigest.getInstance("MD5"); 
			md.update(plainText.getBytes()); 
			byte b[] = md.digest(); 
			int i; 
			StringBuffer buf = new StringBuffer(""); 
			for (int offset = 0; offset < b.length; offset++) { 
				i = b[offset]; 
				if(i<0) i+= 256; 
				if(i<16) 
				buf.append("0"); 
				buf.append(Integer.toHexString(i)); 
			}
			return  buf.toString();
		} catch (NoSuchAlgorithmException e) { 
			e.printStackTrace(); 
		}
		return plainText; 
	}
	
	/**
	 * 删除字符中的空格
	 * @param str  输入字符串
	 * @return
	 */
	public static String deleteIt(String str) {
        String newStr = "";
        if (str != null) {
        	newStr = str.replaceAll(" ", "");
        }
        return newStr;
    }
	
	public static boolean isEmail(String str) {
		String regex = "^[\\w-]+(\\.[\\w-]+)*@[\\w-]+(\\.[\\w-]+)+$";
		return match(regex, str);
	}
	
	public static boolean isPhoneNum(String str) {
		if (str.isEmpty()) {
			return false;
		}
		String regex = "^[1][3-8]+\\d{9}";
		return match(regex, str);
	}
	
	/**
	 * 检测字符串中是否含有指定的字符串
	 * 
	 * @param original	源字符串
	 * @param targete	匹配字符串
	 * @return
	 */
	public static boolean isHave(String original, String targete) {
		String regex = targete;
		return find(regex, original);
	}
	
	/**
	 * 通过 一个key值，使用HmacSHA1算法加密字符串
	 * @param text
	 * @param keyBytes
	 * @return
	 */
	public static String hashHmac(long text, String keyBytes) {
		return hashHmac(text + "", keyBytes);
	}
	
	public static String hashHmac(String text, String keyBytes){
		Mac hmacSha1 = null;
		try {
			  hmacSha1 = Mac.getInstance("HmacSHA1");
		}catch (NoSuchAlgorithmException nsae) {
			 try {
				hmacSha1 = Mac.getInstance("HMAC-SHA-1");
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}
		}
		SecretKeySpec macKey = new SecretKeySpec(keyBytes.getBytes(), "HmacSHA1");
		try {
			hmacSha1.init(macKey);
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		}
		byte b[] = hmacSha1.doFinal(text.getBytes());
		StringBuffer buf = new StringBuffer(""); 
		int i; 
		for (int offset = 0; offset < b.length; offset++) { 
			i = b[offset]; 
			if(i<0) i+= 256; 
			if(i<16) 
			buf.append("0"); 
			buf.append(Integer.toHexString(i)); 
		}
		return buf.toString();
	}
	
	/** 
     * 根据手机的分辨率从 dp 的单位 转成为 px(像素) 
     */  
    public static int dipTopx(Context context, float dpValue) {  
        final float scale = context.getResources().getDisplayMetrics().density;  
        return (int) (dpValue * scale + 0.5f);  
    }  
  
    /** 
     * 根据手机的分辨率从 px(像素) 的单位 转成为 dp 
     */  
    public static int pxTodip(Context context, float pxValue) {  
        final float scale = context.getResources().getDisplayMetrics().density;  
        return (int) (pxValue / scale + 0.5f);  
    } 
    
  	private static boolean match(String regex, String str) {
		try {
			Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
			Matcher matcher = pattern.matcher(str);
			return matcher.matches();
		}catch (PatternSyntaxException e) {
		}
		return false;
	}
	
	private static boolean find(String regex, String str) {
		try {
			Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
			Matcher matcher = pattern.matcher(str);
			while(matcher.find()){
				return true;
			}
		} catch (PatternSyntaxException e) {
		}
		return false;
	}
}
