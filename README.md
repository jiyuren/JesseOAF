# 欢迎使用Jesse Open Assist Frame

####JOAF是一个Android的轻量级辅助应用开发框架，把自己日常反复使用的东西精炼出来，欢迎讨论反馈。

- **集中工具** ：将日常开发中常用的系统的繁琐方法进行了二次封装，以达到简易使用的目的。并且增加了一部分拓展功能。当然这都是基于我日前的工作进行的封装，简单但局限。
- **开源拓展** ：集合了非常优秀的国内外常用的开源工具，并且在不断丰富，以便满足一个lib包，就包含所有资源，不用再去网上找来找去浪费时间

> ####本框架集合了以下优秀资源，在此向这些优秀的开发者致敬！
> 作者：Jeremy Feinstein：
[JazzyViewPager](https://github.com/jfeinstein10/JazzyViewPager)
一个ViewPager切换特效的开源控件，效果很丰富很好用

>[SlidingMenu](https://github.com/jfeinstein10/SlidingMenu)
提供侧滑打开侧边栏的开源控件，功能超级强大，很出名，很多人用，这里不就多介绍了。


>作者：Chris Banes:
[PullToRefresh](https://github.com/chrisbanes/Android-PullToRefresh)
提供多种刷新，可以嵌套只几乎所有控件中使用，四个方向都可以滑动刷新，其中有几个控件有小bug


-------------------

### JOAF总体简介
JOAF总体思想是保持代码的最大简洁与复用。

####一、Util包
>1.将所有与试图有关的常用操作全部至于**ViewUtill**类中。该类集中了**Toast**，**Log**，**弹窗**，**Activity跳转** ，**等待提示**，**退出应用**一系列功能。该类中的所有方法均为静态，可以在任何地方调用，并且不需要手动传入Context。

>2.将常用的与App有关的方法放入AppUtil中，目前包含了获取通过Context获取**版本号**，**版本名称**，**APPIcon**, **是否在前台运行** 等功能。

####二、Base包
>1、目前包含3个base类，使用该框架前提是所有Activity（包括FragmentActivity）`必须继承`自该包中的BaseActivity或者BaseFragmentActivity。主要是在这两个Base类中，重写了onCreate，onResume，onDestroy方法，来记录或删除当前交互的Activity的Context。

#### 代码块
```
@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_base);
		ViewUtil.setContext(this);
		registerReceiver();
	}
	
	@Override
    public void onResume() {
        super.onResume();
        checkGuider();
        ViewUtil.setCurrentContext(this);
    }
    
    	@Override
    protected void onDestroy() {
		ViewUtil.removeContext(this);
        ViewUtil.hideKeyBoard();
        this.unregisterReceiver(mReceiver);
        super.onDestroy();
    }
```
当然，如果没有继承Base也没关系，`可以手动在自己的Activity中，向ViewUtil添加context`,以保证ViewUtil的正常使用。Base类还提供了手势右滑返回功能，和网络状态监视功能。

>2、ManagerBase,继承自Observable。该类为单例使用，所有单例、管理类继承ManagerBase后，重写其中releaseManager，与resetPool方法，用来清除单例保存对象。清除操作在调用ViewUtil的exitSystem方法后进行。

####三、通知总线
>ObserverBus. 该通知总线是观察者的集合。具体使用方法见代码注释


### JOAF自定义模块

####一、AsyncImage 异步网络图片读取并换成
>1.AsyncImageView
该类是用来代替系统ImageView的自定义组件，提供了通过Url，或指定Key值，直接下载，缓存，存储，读取图片的功能，通过AsyncImageLoader类实现。具体使用方法见代吗注释

>2.AsyncImageLoader
为图片读取，下载，缓存读写类，使用了LruCache作为内存换成，与本地SD卡缓存，多线程操作。默认本地缓存目录为/Android/data/包名，默认图片单独下载地址为/Photo/包名。当然，按照需求可以自定义路径，继承AsyncImagePathDefine，并重写就可以完成。

>3.AsyncImagePathDefine
配合AsyncImageLoader使用的路径定义类，继承重写可更改路径

>4.AsyncImageScale为图片缩放类
只需要传入图片和需要缩放的宽高既可以完成（完善中）

>5.AsyncImageviewSetviewComplete
配合AsyncImageView使用的图片读取状态接口

####二、SuperButton 多功能状态按钮
>多功能按钮，无须使用<selector>或者<style>即可使用基本的按下效果，支持纯文字按钮，矩形按钮，矩形缩放按钮，图形背景按钮。和系统Button使用方法基本相同，不同的是在findViewById之后，在使用前，按照需求使用不同的init方法初始化按钮类型。

####三、CustomDialog  自定义对话框
>替代系统丑陋的黑色白健对话框，提供单键和双键显示，支持图片显示。统一风格，简洁大方。

####四、SwipeListView  横向滑动列表
>提供类似于IOS中最早出现的列表Item横向滑动展开功能，滑动展开view可以自定义，使用非常方便，只需要按照规则创建就好了

####五、VerticalViewPager  纵向滑动ViewPager (摘自网络具体是谁做的，传说是台湾人)
>可以纵向滑动的Viewpage,支持view和Fragemnt，使用方法和系统自带的一模一样，无须多言。不一样的地方就是我加入了过度动画的速度修正,在FixedSpeedScroller中直接定义就好了
```
	private int mDuration = 750;
```

####六、PullToRefreshView  上下啦刷新
>一个上下啦刷新控件，可以自定义刷新提示，title等，如果不需要底部上啦刷新也可以关掉，可以嵌套至多种空间外层使用。

自定义提示语
```
public void setHeadText(String refreshPullText, String refreshPullReleaseText, 
			String refreshingText, String refreshPullDesc) {
		this.refreshPullText = refreshPullText;
		this.refreshPullReleaseText = refreshPullReleaseText;
		this.refreshingText = refreshingText;
		this.refreshPullDesc = refreshPullDesc;
	}
	
	public void setFootText(String refreshFootText, String refreshFootReleaseText, 
			String footRefreshingText, String refreshFootDesc) {
		this.refreshFootText = refreshFootText;
		this.refreshFootReleaseText = refreshFootReleaseText;
		this.footRefreshingText = footRefreshingText;
		this.refreshFootDesc = refreshFootDesc;	
	}
	
```	


```
/**
	 * 取消底部刷新
	 */
	public void withOutFootRefresh() {
		footRefresh = false;	
	}
```

####七、BlurView  动态模糊
>类似于IOS和Windows7 中使用的经典的模糊特效（毛玻璃效果）可以是整个界面立马显得高大上起来。
使用方法如下：
```
    BlurView blurView = new BlurView(context, 13);
	blurView.updateBlurView(targetBlurView, backgroundView);
```
其中数字13表示模糊的效果值范围0~25
targetBlurView为需要被模糊的view，backgroundView为背景view。
如果是list或者任何滚动的view中需要使用模糊，则需要在Scoller监听器中去不断调用updateBlurView方法。

### JOAF更新日志
>V1.0
        初始版本
        
>V1.1
        增加BlurView动态模糊类
        修复ViewLoader中读取缓存数量异常的bug


### 反馈与建议

- 邮箱：<jeissie@jeissie.com>